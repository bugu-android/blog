---
title: android 多次启动app问题
subtitle: android 多次启动app问题
date: 2019-06-26
tags: ["多次启动"]
---

## android 多次启动app问题
### 场景一

>1.启动app
>
>2.按back退出app
>
>3.后台推送
>
>4.收到推送通知
>
>5.点击通知，启动app

        Intent intent = context.getPacka().getLaunchIntentForPackage(context.getPackageName());
        context.startActivity(intent);

>6.进入app主页面
>
>7.点击HOME键
>
>8.点击桌面app图标
>
>9.App又重新启动了

### 场景二
>1.手机上下好apk

>2.点击apk安装

>3.安装好提示 完成 打开时，选择打开app

>4.app经过【SplashActivity】 跳转到 【MainActivity】

>5.点击home

>6.点击app图标打开app

>7.app又打开了【SplashActivity】

### 问题
两次场景都出现这样一个问题，打开app后，点击home键，再次点击App图标，我们想到的正常情况是回到App刚才打开的页面，事实情况却是又一次打开了SplashActivity页面,好似App又重启了一样。这种类似问题好多客户也多次提出过。

### 解决
通过分析发现，出现问题的地方在于我们启动app的地方，一个是收到通知时代码启动app，一个是安装完成后直接打开app。而我们正常打开app是没有问题的。

而是我尝试在App入口SplashActivity#onCreate()里打印了一个LOG,以便查看intent

    Log.i(TAG, getIntent().toString());

分别记录多种情况下，intent是否有什么不同


- 正常点击App图标App

> 05-10 13:59:27.421 10551-10551/com.bugull.navigate.dealer I/Splash: Intent { act=android.intent.action.MAIN cat=[android.intent.category.LAUNCHER] flg=**0x10200000** cmp=com.bugull.navigate.dealer/.module.login.SplashActivity bnds=[820,689][1002,871] (has extras) }

- 收到通知启动App

> 05-10 14:00:44.034 10551-10551/com.bugull.navigate.dealer I/Splash: Intent { act=android.intent.action.MAIN cat=[android.intent.category.LAUNCHER] flg=**0x10000000** pkg=com.bugull.navigate.dealer cmp=com.bugull.navigate.dealer/.module.login.SplashActivity }

- 安装后直接打开App

> 06-26 14:34:16.852 31784-31784/? I/_TAG_: Intent { act=android.intent.action.MAIN cat=[android.intent.category.LAUNCHER] flg=**0x10000000** pkg=com.bugull.qinyuan cmp=com.bugull.qinyuan/.ui.user.activity.SplashActivity }

- 点击HOME后点击app图标

> 05-10 14:01:06.469 10551-10551/com.bugull.navigate.dealer I/Splash: Intent { act=android.intent.action.MAIN cat=[android.intent.category.LAUNCHER] flg=**0x10600000** cmp=com.bugull.navigate.dealer/.module.login.SplashActivity bnds=[820,689][1002,871] (has extras) }

### 比较
对比发现正常情况下的intent的Flag值为0x10200000，其他两种情况的flag值为0x10000000，导致点击home后的flag值为0x10600000，似乎有一些关系。

查看了一下Intent的Flag

    public static final int FLAG_ACTIVITY_NEW_TASK = 0x10000000;

    public static final int FLAG_ACTIVITY_RESET_TASK_IF_NEEDED = 0x00200000;

    public static final int FLAG_ACTIVITY_BROUGHT_TO_FRONT = 0x00400000;

而且正常启动的时候pkg是没有值的，而两次异常打开app都有pkg值。

而是想到，我们可不可以模拟一个正常打开的intent，替换掉异常的intent呢？而是我修改了点击通知栏唤起app的地方代码

代码一：

     Intent intent = context.getPacka().getLaunchIntentForPackage(context.getPackageName());
	 intent.setPackage(null)
     context.startActivity(intent);

代码二：
    
    Intent intent = new Intent(context,SplashActivity.class);
	intent.setAction("android.intent.action.MAIN");
	intent.addCategory("android.intent.category.LAUNCHER");
	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	context.getInstance().startActivity(intent);

测试发现是可以解决场景一的问题的，但是场景二，打开app代码是系统的，不能修改，此方法就行不通了，而且可能还有其他途径导致的intent不一致。伴随着网上查找答案，和多次测试发现，每次启动只是启动了SplashActivity,我们可以过滤掉这种异常的intent#Flag，让这个页面自己关闭掉。

    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
       	 	finish();
        	return;
		}
	}

[https://stackoverflow.com/questions/23880318/how-does-the-android-home-screen-launcher-restore-tasks](https://stackoverflow.com/questions/23880318/how-does-the-android-home-screen-launcher-restore-tasks "stackoverflow")

问题是解决了，但是还是一头雾水。而是寻找了一些文章和资料，终于有了一点头绪。（以下摘选）

## 探索

首先从App启动说起，当我们点击app图标的时候，是通过桌面（即Launcher，也是一个APP）startActivity传入一个特定的Intent，然后由ActivityManagerServer发动启动任务的，而启动过后的Activity都是用一个栈Task来保存管理的。

#### App切换
现在桌面Launcher有两个App：A和B，两个app都为一个Task来保存里面所有的Activity。看看App之间是怎么切换的。其中一个【App:activity1-activity-2】代表每个app的Task的情况。

1.进入桌面，打开默认页面L1

>【L：L1】
>
> 显示L1

2.打开 A,依次打开页面A1,A2

>【L:L1】【A:A1-A2】
>
> 显示A2

3.点击Home键，把Launcher的Task移到最顶端

>【A：A1-A2】【L：L1】
>
> 显示L1 

4.打开B，依次打开B1,B2

>【A：A1-A2】【L:L1】【B：B1-B2】
> 
> 显示B2

5.点击Home键，把Launcher的Task移到最顶端
>【A：A1-A2】【B：B1-B2】【L:L1】
> 
> 显示L1

6.打开A
>【B：B1-B2】【L:L1】【A：A1-A2】
> 
> 显示A2

查询资料可知
>我们编写任何一个Activity的时候，都可以在AndroidManifest里面显式指定一个taskAffinity的属性，也就是说该Activity归属于对应taskAffinity的栈；如果没有指定任何taskAffinity，那么该Activity将会直接归属于包名所在的Task之下。而我们启动一个Activity时（这里只讨论standard启动模式），那么回去先搜寻对应的Task是否存在，如果不存在，新建一个Task并将Activity入栈，如果已经存在对应的Task，那么直接在对应Task入栈即可。

所以当第6步再次打开A时，先会检查A对应的Task存不存在，如果存在，直接把A对应的Task移动到栈顶，所以会显示A2。

那为什么A为什么不会重启呢？

#### 桌面的启动管理

AndroidManifest这个文件，我们轻而易举发现，但凡是App入口Activity，那么一定会包含 

    <intent-filter>
    <action android:name="android.intent.action.MAIN" />
    <category android:name="android.intent.category.LAUNCHER" />
	</intent-filter>
这个就是跟桌面约定好的启动拦截过滤器。因为桌面有一个很明显的需求就是，如果我们再次点击已经在后台的App图标时，是应该将该后台任务挪到前台而不是再次启动该App程序

而从柯元旦所著的《android内核剖析》一书中有记录如下规则：
>**每次启动Intent导致新创建Task的时候，该Task会记录导致其创建的Intent；而如果后续需要有一个新的与创建Intent完全一致（完全一致定位为：启动类，action、category等等全部一样，不可多项也不可缺少），那么该Intent并不会触发Activity的新建启动，而只会将已经存在的对应Task移到前台；这也就是为什么桌面会在再次点击图标时将后台任务挪到前台而不是重新启动App的实现**


以上所述也验证了我们开始解决问题的猜想，重新启动App#SplashActivity的原因：

>两种异常启动App时的intent不一致，导致重新启动SplashActivity，而不是将打开的页面移到前台。

#### 解决bug
1、启动Intent的设置和桌面启动Intent保持完全一致。（有些地方无法实现）

2、自身业务代码规避，我们可以知道，如果是多余的闪屏页入口Activity的话，其基本不可能位于Task的根部，而如果正常启动的话，闪屏页入口Activity必定在多对应的Task的根部位置，那么我们可以从这个地方对于这个bug进行规避，方法就是在闪屏页入口Activity的onCreate代码加入如下一段代码：


    // 避免从桌面启动程序后，会重新实例化入口类的activity
	if (!this.isTaskRoot()) {
	    Intent intent = getIntent();
	    if (intent != null) {
	        String action = intent.getAction();
	        if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(action)) {
	            finish();
	            return;
	        }
	    }
	}


参考：

> [https://www.cnblogs.com/net168/p/5722752.html](https://www.cnblogs.com/net168/p/5722752.html "参考")
