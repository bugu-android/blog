---
title: 私有库教程
subtitle: 教程
date: 2019-11-20
tags: ["教程"]
---

## 第一步

整理出代码，并制作成Module。
clone bugu-android 的git项目，把Module导入Skeleton,并提交上传代码。
打开 Android Studio 右上角 gradle -> "你的Module" -> assemble 双击。
把文件模式转换成Project -> 打开"你的Module" -> build -> output -> "你的。Module"-release.aar -> 右键在文件中打开 获得aar文件。

## 第二步

为你的依赖库想一个合适的项目名称 和 版本号（一般为1.0.0）
把 aar 文件 和 【compile group:'com.bugull', name:'项目名称', version:'版本号'】项目信息 发送给温总。 由温总添加到私有Maven库。
可以在大群里找到温总的QQ私聊。

## 第三步

首先在app 中的 build.gradle配置账号密码和私有库连接 具体看文件

https://gitlab.yunext.com/root/bugu-android/blob/master/README.md

```
repositories{
		mavenLocal()
		maven {
    		credentials {
        		username "**"
        		password "**"
    		}
    		url "http://maven.****"
		}
		mavenCentral()

	}
```
然后在对应的 module 中的 build.gradle 添加依赖

```
dependencies {
    ...
    implementation 'com.bugull:项目名:版本号@aar'
  
    ...
}
```

## 第四步

把使用文档编辑上传到 bugu-android blog 【 https://gitlab.com/bugu-android/blog.git 】 或者bugu-android yuntext git 【 https://gitlab.yunext.com/root/bugu-android.git 】