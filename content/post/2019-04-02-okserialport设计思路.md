---
title: okserialport设计思路
subtitle: 如何封装一个高内聚低藕合的工具类
date: 2019-04-02
tags: ["设计思路","高内聚低藕合"]
---


## 前期分析

到手的材料只有一个SerialPortUtil提供了如下方法：

> #### 打开串口
> #### 关闭串口
> #### 发送字节数组
> #### 接收字节数组

我需要提供的数据是

> #### 串口地址
> #### 波特率

在确定了SerialPortUtil安全可用后，我希望可以很好的接入现有的业务

冷柜的业务大概是 下位机不会主动上报，只有收到下发指令后才会上报对应的数据

一收一发，很容易联想到OKHttp，但是串口通信是串行的，所以我计划用队列的形式缓存消息，在收到响应后再发下一条，若是若干时间无响应就当有错误，就跳过。

## 解析重构

虽然在某个项目很适用，但和其他程序员沟通后发现，不够通用，串口通信不一定是一收一发的形式，可能只收不发，也可能只发不收，或者发和收没有必然关系。
所以，这个一收一发的控制 如果可以独立出来，根据不同的项目有不同的实现，那么这个架构就比较通用了。

很快我就想到了用抽象类的方式来做这个事

下面我用一些示意代码来做一些展示


	abstract class Controller{
		var onSerialPortSendAction: OnSerialPortSendAction? = null

		var onSerialPortResponseListener: OnSerialPortResponseListener? = null

		abstract fun onSendArrived(byteArray: ByteArray)

		abstract fun onResponseArrived(byteArray: ByteArray)
	}

`onSerialPortSendAction` 和 `onSerialPortResponseListener` 分别是发送和接收字节数组的操作。
Controller 作为控制器，如果可以拦截所有的收和发操作，并且持有收和发操作的接口，也就意味着它可以控制，什么时候发什么时候收。

而`onSerialPortSendAction` 和 `onSerialPortResponseListener` 的定义也很简单


	interface OnSerialPortSendAction{
    	fun onSendAction(byteArray: ByteArray)
	}
	
	interface OnSerialPortResponseListener{
  	 	fun onResponse(data: ByteArray)
  	}

现在要做的就是在`SerialPortUtil`外面包装一层`SerialPortClient`,让`SerialPortClient`分别实现收发的接口，其实就是client 持有一个 `SerialPortUtil` 在接口中使用 `SerialPortUtil`中的方法。


	class SerialPortClient(path,baudrate):
		 OnSerialPortSendAction, OnSerialPortResponseListener{
	
		private val okSerialPort = SerialPortUtil(path,baudrate,this)
		private var responseListener: OnSerialPortResponseListener? = null
	
		fun setOnSerialPortResponseListener(
			responseListener:OnSerialPortResponseListener){
			this.responseListener = responseListener
		}

		override fun onSendAction(byteArray: ByteArray) {
  		  okSerialPort.sendSerialPort(byteArray)
		}

		override fun onResponse(data: ByteArray) {
   	  onSerialPortResponseListener?.onResponse(data)
		}
	
		fun sendByteArray(byteArray: ByteArray){
			onSendAction(byteArray)
		}

	}


注意， SerialPortUtil的 responselistener 是 SerialPortClient 实现的， 是为了拦截响应，业务实现还有另外一个OnSerialPortResponseListener，它才是真正被响应到业务的接口

那么只要在client里持有一个controller 传入对应的接口，并在发送和响应的地方拦截。那么就可以实现了。


	class SerialPortClient(path,baudrate):
		 OnSerialPortSendAction, OnSerialPortResponseListener{
		...
		private var controller: Controller? = null
	
	fun setController(controller: Controller){
		controller.onSerialPortSendAction = this
		controller.onSerialPortResponseListener = responseListener//这是业务响应的接口
		this.controller = controller
	}
		...
	
	override fun onResponse(data: ByteArray) {
    	if(controller != null)
     	   controller?.onResponseArrived(data)
  		else
        onSerialPortResponseListener?.onResponse(data)
	}
	
		fun sendByteArray(byteArray: ByteArray){
  		  	if(sendController != null)
     		   sendController?.onSendArrived(byteArray)
    		else
     	   	onSendAction(byteArray)
		}
	}


如上 做到了 控制的解偶

## 再走一小步

既然把收 和 发 的操作都抽象成了接口，不如再进一步，利用范型，可以做到收发具体的类，但是类和字节数组的转换涉及到具体的业务，所有再定义2个抽象类，需要调用者自己实现类和字节数组的转换，我们在client中直接调用调用者的实现，即可通过类得到字节数组了


	abstract class SerialPortSendParse<T>{
    abstract fun onParse(data: T): ByteArray
	}
	abstract class SerialPortResponseParse<T>: OnSerialPortResponseListener{
  	  override fun onResponse(data: ByteArray) {
        onResult(onParse(data))
   }

    abstract fun onParse(data: ByteArray): T

    abstract fun onResult(result: T)
	}


`SerialPortResponseParse` 是实现了`OnSerialPortResponseListener`，所以我们不用任何特殊的操作

而发送的操作，之前虽然有个接口，但是和业务无关，所有并不继承`OnSerialPortSendAction`


	class SerialPortClient(path,baudrate):
		 OnSerialPortSendAction, OnSerialPortResponseListener{
	...
	//这里的*代表所有类型，即这个实例针对的是所有类型
	private var onSendParse: SerialPortSendParse<*>? = null
	fun <T> setOnSendParse(onSendParse: SerialPortSendParse<T>){
		this.onSendParse = onSendParse
	}
	
	@Suppress("UNCHECKED_CAST")
	fun <T> send(data: T){
    if(sendParse != null)
        sendByteArray((sendParse as SerialPortSendParse<T>).onParse(data))
    else
        throw Exception("sendParse is null")
	}

	}


额外定义的send 方法， 会做一次 `UNCHECKED_CAST` 需要调用者确认，设置的类型和传入的类型是一致的或是继承关系。
在类型转换后，调用业务实现，得到字节数组，再传入sendByteArray 进行下一步操作。

## 总结


![](https://gitlab.com/bugu-android/blog/raw/master/themes/beautifulhugo/images/okserialport.jpg?inline=false)

通过对SerialPortUtil包一层SerialPortClient，进行一系列的操作而不修改SerialPortUtil
解偶了，发送数据，接收数据，控制收发 3个操作，来达到分离业务和框架的目的。


