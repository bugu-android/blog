 ---
title: Android单元测试
subtitle: Android单元测试简单介绍
date: 2019-06-24
tags: ["AndroidUT"]
---


## 主要内容

> #### **Android单元测试**简单介绍
> #### 常用的单元测试框架

## 1 Android单元测试简单介绍

### 1.1 Android单元测试是什么

单元测试是应用程序测试策略中的基本测试，通过对代码进行单元测试，可以轻松地验证单个单元的逻辑是否正确，在每次构建之后运行单元测试，可以帮助您快速捕获和修复因代码更改（重构、优化等）带来的回归问题。

### 1.2 Android单元测试的分类

1.本地测试(Local tests): 只在本地机器JVM上运行，以最小化执行时间，这种单元测试不依赖于Android框架，或者即使有依赖，也很方便使用模拟框架来模拟依赖，以达到隔离Android依赖的目的

2.仪器化测试(Instrumented tests): 在真机或模拟器上运行的单元测试，由于需要跑到设备上，比较慢，这些测试可以访问仪器（Android系统）信息，比如被测应用程序的上下文，一般地，依赖不太方便通过模拟框架模拟时采用这种方式。

## 2 常用的单元测试框架

### 2.1 Junit4

Junit是Java单元测试框架，使用断言assert判断测试结果。一般Android Studio新建项目时自动依赖Junit4.X。使用Junit4.x版本进行单元测试时，不用测试类继承TestCase父类，因为，junit4.x全面引入了Annotation来执行我们编写的测试。

Junit4.x版本我们常用的注解:    

> 1.@Before 注解：在每个测试方法之前执行；  
> 2.@After 注解：在每个测试方法之后执行；  
> 3.@BeforeClass 注解：在所有方法执行之前执行；  
> 4.@AfterClass 注解：在所有方法执行之后执行；  
> 5.@Test(timeout = xxx) 注解：设置当前测试方法在一定时间内运行完，否则返回错误；  
> 6.@Test(expected = Exception.class)注解：设置被测试的方法是否有异常抛出。抛出异常类型为：Exception.class；  
> 7.@Ignore 注解：注释掉一个测试方法或一个类，被注释的方法或类，不会被执行。

执行的顺序为:  
@BeforeClass -> @Before -> @Test{方法A} -> @After -> @Before -> @Test{方法B} -> @After -> @AfterClass;
### 2.2 AndroidJunitRunnner
AndroidJUnitRunner，Google官方的android单元测试框架之一，适用于 Android 且与 JUnit 4 兼容的测试运行器！测试运行器可以将测试软件包和要测试的应用加载到设备、运行测试并报告测试结果。
### 2.3 Mockito
什么是Mock？在面向对象程序设计中，模拟对象（英语：mock object，也译作模仿对象）是以可控的方式模拟真实对象行为的假的对象。Mockito是GitHub上使用最广泛的Mock框架,并与JUnit结合使用.Mockito框架可以创建和配置mock对象.使用Mockito简化了具有外部依赖的类的测试开发。

在实际的单元测试中，我们测试的类之间会有或多或少的耦合，导致我们无法顺利的进行测试，这时我们就可以使用Mockito，Mockito库能够Mock对象，替换我们原先依赖的真实对象，这样我们就可以避免外部的影响，只测试本类，得到更准确的结果。
#### 依赖
    testImplementation "org.mockito:mockito-core:2.8.9"

#### 基本使用方法

| 方法名 | 方法描述 |
| ------ | ------ |
| thenReturn(T value) | 设置要返回的值 |
| thenThrow(Throwable... throwables) | 设置要抛出的异常 |
| thenAnswer(Anser<?> answer)   | 对结果进行拦截  |
| doReturn(Object toBeReturned)   | 提前设置要返回的值   |
| doThrow(Throwable... throwables)   | 提前设置要抛出的异常  |
| doAnswer   | 提前对结果进行拦截   |
| doCallReallMethod()   | 调用某一个方法真实的实现   |
| doNothing()   | 设置void   |

#### 基本语法
> 1.when(mock.someMethod()).thenReturn(toBeReturned)  
> 2.when(mock.someMethod(anyString())).thenThrow(new RuntimeException()).thenReturn(toBeReturned)  
> 3.doReturn(10).when(mock).someMethod()  
> 4.doThrow(new RuntimeException()).when(mock).someMethod()  
> 5.doAnswer(answer).when(mock).someMethod()  
> 6.doNothing().when(mock).someMethod()  
> 7.doCallReallMethod().when(mock).someMethod()

附：  

> [Mockito中文文档](https://github.com/hehonghui/mockito-doc-zh)

### 2.4 PowerMock
PowerMock是一个Java模拟框架，可用于解决通常认为很难甚至无法测试的测试问题。使用PowerMock，可以模拟静态方法，删除静态初始化程序，允许模拟而不依赖于注入，等等。PowerMock通过在执行测试时在运行时修改字节码来完成这些技巧。PowerMock还包含一些实用程序，可让您更轻松地访问对象的内部状态。
#### 依赖
    testImplementation "org.powermock:powermock-module-junit4:1.7.4"
    testImplementation "org.powermock:powermock-module-junit4-rule:1.7.4"
    testImplementation "org.powermock:powermock-api-mockito2:1.7.4"
    testImplementation "org.powermock:powermock-classloading-xstream:1.7.4"
#### 私有方法

    PowerMockito.doReturn(Object toBeReturned)
        .when(T mock,String methodToExpect, Object... arguments) throws Exception;
#### 静态方法

    PowerMockito.mockStatic(Class<?> type,Class<?>... types);
    PowerMockito.when(mock.someMethod()).thenReturn(Object toBeReturned);

#### 静态变量

    Whitebox.setInternalState(Object object, String fieldName, Object value);

### 2.5 robolectric
Java单元测试本地测试是无法调用到Android API的，那么，我们既要使用本地测试，但测试过程又难免遇到调用系统API那怎么办？其中一个方法就是mock objects，比如借助Mockito，另外一种方式就是使用Robolectric。
Robolectric就是为解决这个问题而生的。它实现一套JVM能运行的Android代码，然后在unit test运行的时候去截取android相关的代码调用，然后转到他们的他们实现的Shadow代码去执行这个调用的过程。
#### 依赖
    testImplementation "org.robolectric:robolectric:3.8"
如需调用Android代码需要在build.gradle文件中添加

    android{
        testOptions{
            unitTests{
                inclideAndroidResources = true
            }
        }
    }
#### shadow类
##### Android shadow类
框架针对Android sdk中的类提供了很多Shadow类例如：ShadowActivity、ShadowService、ShadowTextView、ShadowAlertDialog、ShadowWifiManager等等，这些可以方便我们对Android的相关对象进行测试

##### 自定义shadow类

###### 首先创建原始类
    public class Test{
        private int id;
        public Test(int id,String test1,String test2){
            this.id=id;
        }
        public int getId(){
            return id;
        }
    }

###### 创建Shadow类
    @Implements(Test.class)
    public class ShadowTest{
        @RealObject
        Test test
        @Implementation
        public int getId(){
            return 11;
        }
    }
###### 测试使用Shadow类
    @RunWith(RobolectricTestRunner.class)
    @Config(constants = BuildConfig.class,
        sdk = 23,
        shadows = {ShadowTest.class})
    public class TestShadowTest {
        @Before
        public void setUp(){
            ShadowLog.stream = System.out;
        }

        @Test
        public void ShadowTest(){
            Test test = new test(15);
            Log.d("test", String.valueOf(data.getId()));

            ShadowTest shadowTest = extract(test);
            assertEquals(11, shadowData.getId());
        }
    }


#### @Config配置
>配置sdk版本(sdk)  
>配置application(application)  
>配置资源文件和Assert路径(resourceDir和assetDir)  
>配置限定符(qualifiers)  
>配置菜单文件(manifest)  
>配置Gradle生成BuildConfig类(constants)  
>配置shadow类(shadows)  
>配置Android Libraries(libraries)  

#### 追加模块
如果需要调用则可添加依赖

| SDK Package | Robolectric Add-On Package |
| :------: | :------: |
| com.android.support.support-v4  | org.robolectric:shadows-support-v4  |
| com.android.support.multidex | org.robolectric:shadows-multidex  |
| com.google.android.gms:play-services  | org.robolectric:shadows-play-services  |
| com.google.android.maps:maps  | org.robolectric:shadows-maps  |
| org.apache.httpcomponents:httpclient | org.robolectric:shadows-httpclient  |

#### 生命周期控制controller

##### activityController
    @RunWith(RobolectricTestRunner.class)
    @Config(constants = BuildConfig.class,sdk = 23)
    public class ActivityUnitTest{
        private activityController<Activity> controller;
        @BeforeClass
        public void setUp(){
            controller = Robolectric.buildActivity(Activity.class);
        }
        @Test
        public void testActivity(){
            controller.create();
            controller.resume();
            controller.pause();
            controller.stop();
            controller.destroy();
        }
    }
#### serviceController
    @RunWith(RobolectricTestRunner.class)
    @Config(constants = BuildConfig.class, sdk = 23)
    public class ServiceUnitTest {
        private ServiceController<TestService> controller;

    @BeforeClass
    public void setUp(){
        controller = Robolectric.buildService(TestService.class);
    }

    @Test
    public void testServiceTest(){
        controller.create();
        controller.startCommand(0,0);
        controller.bind();
        controller.unbind();
        controller.destroy();
        }
    }

### 2.6 PowerMock
PowerMock是用于模拟http请求的开源依赖库，使用方便快捷
#### 依赖
    testImplementation 'com.github.andrzejchm.RESTMock:android:0.3.1'
#### 使用方法
##### 首先
    RESTMockServerStarter.startSync(new JVMFileParser());
##### 然后
将请求Url修改为*RESTMockServer.getUrl()*
##### 最后
以Get方法为例

    RESTMockServer.whenGet(pathEndsWith(String urlPart))
        .delay(TimeUnit timeUnit,long... delays) //模拟响应时长
        .thenReturnString(int responseCode, String... responseStrings);


