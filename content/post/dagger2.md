---
title: dagger2
subtitle: qian tan浅谈dagger2
date: 2019-07-05
tags: ["dagger2"]
---

## 目录
- 反射和注解
- Dagger2的导入
- Dagger2的简单使用
- Dagger2的原理
## 反射和注解
1. 反射：
	反射就是把java类中的各种成分映射成一个个的Java对象
	例如：一个类有：成员变量、方法、构造方法、包等等信息，利用反射技术可以对一个类进行解剖，把个个组成部分映射成一个个对象。
     （其实：一个类中这些成员方法、构造方法、在加入类中都有一个类来描述）
	如图是类的正常加载过程：反射的原理在于class对象。
	熟悉一下加载的时候：Class对象的由来是将class文件读入内存，并为之创建一个Class对象。
	[![](https://i.loli.net/2019/07/02/5d1b2aac74b3c28614.jpg)](https://i.loli.net/2019/07/02/5d1b2aac74b3c28614.jpg)
2. 反射常用方法
	```
	Class类的方法
	    /**
     * 包名加类名
     */
    public String getName();

    /**
     * 类名
     */
    public String getSimpleName();

    /**
     * 返回当前类和父类层次的public构造方法
     */
    public Constructor<?>[] getConstructors();

    /**
     * 返回当前类所有的构造方法(public、private和protected)
     * 不包括父类
     */
    public Constructor<?>[] getDeclaredConstructors();

    /**
     * 返回当前类所有public的字段，包括父类
     */
    public Field[] getFields();

    /**
     * 返回当前类所有申明的字段，即包括public、private和protected，
     * 不包括父类
     */
    public native Field[] getDeclaredFields();

    /**
     * 返回当前类所有public的方法，包括父类
     */
    public Method[] getMethods();

    /**
     * 返回当前类所有的方法，即包括public、private和protected，
     * 不包括父类
     */
    public Method[] getDeclaredMethods();

    /**
     * 获取局部或匿名内部类在定义时所在的方法
     */
    public Method getEnclosingMethod();

    /**
     * 获取当前类的包
     */
    public Package getPackage();

    /**
     * 获取当前类的包名
     */
    public String getPackageName$();

    /**
     * 获取当前类的直接超类的 Type
     */
    public Type getGenericSuperclass();

    /**
     * 返回当前类直接实现的接口.不包含泛型参数信息
     */
    public Class<?>[] getInterfaces();

    /**
     * 返回当前类的修饰符，public,private,protected
     */
    public int getModifiers();
	```

3. 以下是测试类方法的例子（使用）
```
public class Person {
    private String name;
    private int age;
    private boolean sex;
    private static final String TAG=Person.class.getSimpleName();
    public static String TAG2="TAG2";
    private List<Student> students;

    private static void printPerson(){
        System.out.println("printPerson");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void printName(){
        System.out.println(name);
    }

    public void printName(String name){
        System.out.println(name);
    }
}
```
---
```
    public static void testMethod(Object object){
        if(!(object instanceof Person)){
            throw new IllegalArgumentException("object must be Person instance");
        }

        Class<?> clazz=object.getClass();

        //测试单个method
        try {
            //无参
            Method method=clazz.getDeclaredMethod("printName",null);
            method.setAccessible(true);
            method.invoke(object,null);
            //有参
            Method method1=clazz.getDeclaredMethod("printName",String.class);
            method1.setAccessible(true);
            method1.invoke(object,"lala");
        } catch (NoSuchMethodException | IllegalAccessException |InvocationTargetException e) {
            e.printStackTrace();
        }

        //测试所有的method，测试返回值
        Method[] methods=clazz.getDeclaredMethods();
        for(Method m:methods){
            m.setAccessible(true);
            if(m.getName().equals("setAge")){
                try {
                    m.invoke(object,17);
                } catch (IllegalAccessException |InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            if(m.getName().equals("getAge")){
                try {
                    int age= (int) m.invoke(object,null);
                    System.out.println("age:"+age);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }
```
以下是测试字段的例子
```
public class TestField {

    public static void main(String[] args){
        getClazz();
        field(new Person());
    }

    /**
     * Type接口 Class类也实现了该接口
     * ParameterizedType    getRawType:获取外层类型，例如List，getActualTypeArguments():Type[],内层类型，例如String
     * getFields():获取公开的字段
     * getDeclaredFields():获取所有的字段
     * @param object
     */
    public static void field(Object object){
        if(!(object instanceof Person)){
            throw new IllegalArgumentException("instance must be Person object");
        }
        //获取单个指定字段名的字段，并设置值
        Class<?> clazz=object.getClass();
        try {
            Field nameField=clazz.getDeclaredField("name");
            nameField.setAccessible(true);
            nameField.set(object,"cj");

        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        //获取所有的字段，并获取值
        Field[] fields=clazz.getDeclaredFields();
        for(Field f:fields){
            try {
                f.setAccessible(true);
                System.out.println(f.getName()+":"+f.getType()+":"+
                        f.get(object));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        //获取所有的字段，筛选出范型
        Field[] mFields=clazz.getDeclaredFields();
        for(Field f:mFields){
            //如果是参数type，意思是类型内含参数,例如Person<String>，List<String>
            if(f.getGenericType() instanceof ParameterizedType){
                ParameterizedType type= (ParameterizedType) f.getGenericType();
                if(type.getRawType() == List.class){
                    if(type.getActualTypeArguments()[0] == Student.class){
                        System.out.println("范型正确，是List<Student>类型");
                    }else if(type.getActualTypeArguments()[0] == String.class){
                        System.out.println("范型正确,是List<String>类型");
                    }
                }
            }
        }
        System.out.println("****************");
    }

    /**
     * 获取class类实例一般有三种：
     *  1。object.getClass()
     *  2。A.class
     *  3。Class.forName(package_path+classname)
     * Class<?>通配符，与Class一致，但是可以避免lint检查
     */
    public static void getClazz(){
        try {
            Class<?> clazz=Class.forName("com.cj.reflect.Person");
            //getSimleName:只获取类名，例如Person
            //getName:获取包名+类名，例如 com.cj.reflect.Person
            System.out.println(clazz.getName());
            System.out.println("****************");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
```

注解的说明：
说明
	例如：
```
		public class MainActivity{
			@BindIt
			TextView tv;
		}
```
@BindIt这个注解是无效的，注解必须有解释器才能运行
>注解解释器的两种形式：运行时注解和编译时注解
a. 运行时注解：
	在运行时刻，通过反射的方式获取到标有指定注解的字段（或者其他），
	然后用处理器注入实例或者进行其他操作。
b. 编译时注解：
	编译的时候扫描代码，处理器获取到标有指定注解的元素，创建java文件，编写处理代码到该文件中，运行时引用该文件中的类进行处理

1. 内容
	* 1.1元注解
		@Retention、@Documented、@Target、@Inherited
		@Retention
		它的取值如下：
			RetentionPolicy.SOURCE 注解只在源码阶段保留，在编译器进行编译时它将被丢弃忽视。 
			RetentionPolicy.CLASS 注解只被保留到编译进行的时候，它并不会被加载到 JVM 中。 
			RetentionPolicy.RUNTIME 注解可以保留到程序运行的时候，它会被加载进入到 JVM 中，所以在程序运行时可以获取到它们。
		```
			@Retention(RetentionPolicy.RUNTIME)
			public @interface TestAnnotation {
				String value()  default  "";
			}
		```
		@Documented
		顾名思义，这个元注解肯定是和文档有关。它的作用是能够将注解中的元素包含到 Javadoc 中去。
		@Target
			Target 是目标的意思，@Target 指定了注解运用的地方
			ElementType.ANNOTATION_TYPE 可以给一个注解进行注解
			ElementType.CONSTRUCTOR 可以给构造方法进行注解
			ElementType.FIELD 可以给属性进行注解
			ElementType.LOCAL_VARIABLE 可以给局部变量进行注解
			ElementType.METHOD 可以给方法进行注解
			ElementType.PACKAGE 可以给一个包进行注解
			ElementType.PARAMETER 可以给一个方法内的参数进行注解
			ElementType.TYPE 可以给一个类型进行注解，比如类、接口、枚举
		```
		//多个注解
		@Target({ElementType.METHOD,ElementType.TYPE})
		public @interface TestAnnotation {
				String value()  default  "";
			}
		/**
		使用注解
		@TestAnnotation(value="myvalue")
		String data;
		**/
		```
		@Inherited
		Inherited 是继承的意思，但是它并不是说注解本身可以继承，而是说如果一个超类被 @Inherited 注解过的注解进行注解的话，那么如果它的子类没有被任何	注解应用的话，那么这个子类就继承了超类的注解。
		
	* 1.2 AnnotatedElement方法
	```
	//反射获取的方法Method,Field,Class,Constructor等都implement AnnotatedElement
   /**
     * 指定类型的注释是否存在于此元素上
     */
    default boolean isAnnotationPresent(Class<? extends Annotation> annotationClass) {
        return getAnnotation(annotationClass) != null;
    }

    /**
     * 返回该元素上存在的指定类型的注解
     */
    <T extends Annotation> T getAnnotation(Class<T> annotationClass);

    /**
     * 返回该元素上存在的所有注解
     */
    Annotation[] getAnnotations();

    /**
     * 返回该元素指定类型的注解
     */
    default <T extends Annotation> T[] getAnnotationsByType(Class<T> annotationClass) {
        return AnnotatedElements.getDirectOrIndirectAnnotationsByType(this, annotationClass);
    }

    /**
     * 返回直接存在与该元素上的所有注释(父类里面的不算)
     */
    default <T extends Annotation> T getDeclaredAnnotation(Class<T> annotationClass) {
        Objects.requireNonNull(annotationClass);
        // Loop over all directly-present annotations looking for a matching one
        for (Annotation annotation : getDeclaredAnnotations()) {
            if (annotationClass.equals(annotation.annotationType())) {
                // More robust to do a dynamic cast at runtime instead
                // of compile-time only.
                return annotationClass.cast(annotation);
            }
        }
        return null;
    }

    /**
     * 返回直接存在该元素岸上某类型的注释
     */
    default <T extends Annotation> T[] getDeclaredAnnotationsByType(Class<T> annotationClass) {
        return AnnotatedElements.getDirectOrIndirectAnnotationsByType(this, annotationClass);
    }

    /**
     * 返回直接存在与该元素上的所有注释
     */
    Annotation[] getDeclaredAnnotations();
	```

2. 注解使用
	* 1创建自己的注解
	```
	//多个注解
	@Target({ElementType.METHOD,ElementType.TYPE}) 
	public @interface MyAnnotation {
			String value()  default  ""; //默认值
	}
	```


	* 2注解提取
	```
	//注解
	@Retention(RetentionPolicy.RUNTIME)
	@Target({ElementType.FIELD})
	public @interface AutoWired {
	}
	
	//activity:
	//字段
	@AutoWired
	String idValue;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my);
		AutoWiredProcess.bind(this); //给idValue赋值一个String实例
	}
	//处理器
	
	public class AutoWiredProcess {
		public static void bind(final Object object) {
			Class parentClass = object.getClass();
			Field[] fields = parentClass.getFields();
			for (final Field field : fields) { //遍历object的所有字段
				AutoWired autoWiredAnnotation = field.getAnnotation(AutoWired.class); //获取字段上的注解
				if (autoWiredAnnotation != null) { //如果存在该AutoWired注解
					field.setAccessible(true); //设置为可访问，例如private也可以设置为可访问
					try {
						Class<?> autoCreateClass = field.getType(); //获取字段的类对象，例如上述activity中的idValue为String
						Constructor autoCreateConstructor = autoCreateClass.getConstructor(); //获取类对象的空构造器，例如new String();
						field.set(object, autoCreateConstructor.newInstance()); //给该字段赋值一个该类的新实例，例如idValue=new String();
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}

				}
			}
		}
	}
	```

## Dagger2的导入
```
// Add Dagger dependencies
dependencies {
  compile 'com.google.dagger:dagger:2.x'
  annotationProcessor 'com.google.dagger:dagger-compiler:2.x'
}
```
If you're using classes in dagger.android you'll also want to include:
```
compile 'com.google.dagger:dagger-android:2.x'
compile 'com.google.dagger:dagger-android-support:2.x' // if you use the support libraries
annotationProcessor 'com.google.dagger:dagger-android-processor:2.x'
```
## Dagger2的使用
使用编译时注解完成对象注入
	主要组成部分：
	1.需要注入的类
	2.Component:是一个接口，注解处理器会在编译期间生成实现该接口的java文件
	3.实例提供者：
		简单对象可以用@Inject直接标注构造方法
		接口对象使用module来提供实例，如果有多个实现者，则需要一个额外的注解
			来区分，例如@Qualifier(“a1”),@Qualifier(“a2”)区分

>简单使用：
	1提供

		public class ApiProvider{
			@Inject
			public ApiProvider(){}
			Observable<HttpResult<UserInfo>> getUserInfo(String username,String pwd){
				if(username==null || pwd==null || "".equals(username) || "".equalse(pwd)){
					return Obserbable.error(new Exception("..."));
				}
				return Api.getUserInfo(username,pwd)
								.scheduleOn(Schedulers.io())
								.observableOn(AndroidScheduler.mainThread());
			}
		}

>2.注射器
```
	public interface AppComponent {
    	void bind(XViewModel xvm);
		void bind(XActivity xActivity);
	}
```
>3.使用
```
	@Inject
	ApiProvider apiProvider;
	void method(){
		DaggerAppComponent.builder().build().bind(this);
	}
```

