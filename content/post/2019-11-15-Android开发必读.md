---
title: Android开发必读
subtitle: 必读！必读！必读！
date: 2019-11-15
tags: ["必读"]
---

## 开发前必读

|项目|内容|
|---|---|
|开发语言|[Kotlin](https://www.kotlincn.net/docs/reference/native-overview.html)&[Kotlin标准库列表](https://kotlinlang.org/api/latest/jvm/stdlib/index.html)|
|如何去组织项目结构|[编程规范](https://bugu-android.gitlab.io/blog/post/2019-09-20-android编程规范/)|
|如何制作本公司的私有依赖库和调用私有依赖库|[私有库教程](https://bugu-android.gitlab.io/blog/post/2019-11-20-私有库教程/)|
|如何正确规范的使用git仓库|[git使用规范](https://bugu-android.gitlab.io/blog/post/2019-11-20-git使用规范/)|

## 私有依赖库版本

私有库的源码参见 https://gitlab.yunext.com/root/bugu-android.git  （ https://gitlab.yunext.com/root/bugu-android ）

|项目|版本|文档|
|---|---|---|
|MQTT通信|com.bugull:okmqtt:1.0.1@aar|[链接](https://bugu-android.gitlab.io/blog/post/2019-11-21-mqtt通信使用文档/)|
|串口通信|com.bugull:okserialport:1.0.2@aar|[链接](https://bugu-android.gitlab.io/blog/post/2019-07-01-串口通信文档/)|
|livedata retrofit2 适配|com.bugull:retrofit2-livedatar-adapter:1.0.2@aar|[链接](https://bugu-android.gitlab.io/blog/post/2019-11-22-livedatar使用文档/)|
|蓝牙通信|com.bugull:bluetoothlink:1.0.3@aar|[链接](https://bugu-android.gitlab.io/blog/post/2019-10-12-蓝牙通信文档/)|

## 可以直接使用的依赖库

在引入外部依赖之前，必须要确认是否安全可用，是否有完善的文档，是否会被长期定时维护。
下面列举已确认可用的外部依赖，欢迎补充。

|项目|说明|文档|
|---|---|---|
|androidx|AndroidX 是 Android 团队用于在 Jetpack 中开发、测试、打包和发布库以及对其进行版本控制的开源项目|[文档链接](https://developer.android.google.cn/jetpack/androidx) & [内置内容和版本](https://developer.android.google.cn/jetpack/androidx/versions?hl=en)|
|RxJava2|通过订阅的方式去解决异步问题|[中文文档](https://mcxiaoke.gitbooks.io/rxdocs/content/)|
|Kotlin协程|可控的异步解决方案【implementation'org.jetbrains.kotlinx:kotlinx-coroutines-core:version'】【implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:version'】|[文档](https://www.kotlincn.net/docs/reference/coroutines-overview.html)&[版本](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.coroutines/index.html)|
|OKHttp|网络请求|[文档](https://square.github.io/okhttp/)|
|Retrofit2|OkHttp的拓展|[文档](https://square.github.io/retrofit/)|
|EventBus|总线事件框架|[文档](http://greenrobot.org/eventbus/)&[版本与依赖](https://github.com/greenrobot/EventBus#add-eventbus-to-your-project)|
