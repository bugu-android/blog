---
title: livedataR使用文档
subtitle: livedataR文档
date: 2019-11-22
tags: ["文档","liveDataR"]
---

## 网络请求工具：LiveDataR

关于网络请求，为兼容LiveData提供了一个解决方案。
（星号替换的信息在内部已经分享）

配置方法：

```

object RetrofitClient {

    private val client = createClient()

    private fun createClient(): Retrofit{
        val clientBuilder = OkHttpClient.Builder()
        //超时的时间设置，根据项目而定。
            .connectTimeout(90000, TimeUnit.MILLISECONDS)
            .readTimeout(90000, TimeUnit.MILLISECONDS)

        val client = clientBuilder.build()

        return Retrofit.Builder()
            .baseUrl("https://*********")
            .addConverterFactory(GsonConverterFactory.create())
            //LiveDataCallAdapterFactory(),添加到Factory中即可
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(client)
            .build()
    }

    fun get() = client
}

```

LiveDataR的使用：

```
	//创建Service
	val service = RetrofitClient.get().create(WeatherApi::class.java)
	
	private val msg = MutableLiveData<String>()
	
	interface WeatherApi {
    	@GET("join") fun join(
        	@Query("access_key") access_key: String = "*****",
        	@Query("app_name") app_name: String = "***"
    	): LiveDataR<WeatherBean<Token>>
	}

	
	private var tokenInvoke: () -> Unit = {}

	fun tokenRetry(){
    	tokenInvoke()
	}


	val tokenLiveData: LiveData<Token> = map(service.join().apply {
		//获取retry方法，便于重新请求
    	tokenInvoke = this::retry
	}){ result ->
    	if(result.isSuccess){
    		//如果成功了，调用getOrNull()获取实体类
        	get(result.getOrNull())
    	}else{ //result.isFailure
    		//请求失败了，调用exceptionOrNull获取错误信息
        postException(result.exceptionOrNull())
        null
    	}
	}
	
private fun postException(exception: Throwable?){
    exception?.printStackTrace()
    msg.postValue(exception?.message)
}

/*这个get 是根据不同项目，不同后台的函数，不通用，这个项目有 success字段，其他项目可能就是判断code，因项目而异。
*/
private fun <T> get(bean: WeatherBean<T>?): T?{
    bean?.apply {
        if(success){
            return data
        }else{
            msg.postValue(error_msg)
        }
    }
    return null
}



```

# 特别注意
# 特别注意
# 特别注意
这个是LiveData对Retrofit的支持，阅读源码可以知道，当你在给这个LiveData添加观察者（Observer）的时候，他就会开始进行网络请求，所以，添加观察者的时机就是第一次进行网络请求的时机。
每个Service生产的LiveData都是单例，所以你的参数很可能依旧保存在内存中！
使用时，请根据需求判断操作：
    
    先修改参数再观察
    观察到数据后就移除观察者
    

